"""Script for scraping down Jenkins jobs configs."""
import os
import pathlib
from urllib.parse import urlparse

import click
import urllib3
from jenkinsapi.jenkins import Jenkins
from tqdm import tqdm

DEFAULT_PATH = os.path.join(os.getcwd(), "jenkins-export")


@click.command()
@click.option("--baseurl", help="jenkins base url", prompt=True)
@click.option("--username", help="jenkins username", prompt=True)
@click.option("--password", help="jenkins password", prompt=True, hide_input=True)
@click.option(
    "--verify/--no-verify", default=True, help="enable/disable SSL verification"
)
@click.option(
    "--output-path",
    help="directory path to write configs",
    prompt=True,
    default=DEFAULT_PATH,
)
def download_configs(baseurl, username, password, verify, output_path):
    hostname = urlparse(baseurl).hostname
    hostname_path = os.path.join(output_path, hostname)
    pathlib.Path(hostname_path).mkdir(parents=True, exist_ok=True)

    if not verify:
        click.secho(
            "verify disabled; also disabling urllib3 warnings", fg="yellow", bold=True
        )
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    jenkins_server = Jenkins(
        baseurl=baseurl, ssl_verify=verify, username=username, password=password
    )

    jobs = jenkins_server.get_jobs()
    for job_name, job in tqdm(jobs, desc="Saving configs"):
        file_path = os.path.join(hostname_path, f"{job_name}.xml")
        with open(file_path, "w") as out_file:
            out_file.write(job.get_config())


if __name__ == "__main__":
    download_configs()  # pylint: disable=no-value-for-parameter
