# jenkins-scraper

Script to download Jenkins jobs configs.

## General usage

```
$ pipenv install
$ pipenv run python scrape.py --help
Usage: scrape.py [OPTIONS]

Options:
  --baseurl TEXT          jenkins base url
  --username TEXT         jenkins username
  --password TEXT         jenkins password
  --verify / --no-verify  enable/disable SSL verification
  --output-path TEXT      directory path to write configs
  --help                  Show this message and exit.
```

